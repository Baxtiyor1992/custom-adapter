package uz.bakhtiyor.begmatov.customadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by Bakhtiyor Begmatov on 1/12/18.
 */

public class CustomAdapter extends ArrayAdapter<RowItem> {

    private final LayoutInflater layoutInflater;

    public CustomAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        layoutInflater = LayoutInflater.from(context);
    }

    public CustomAdapter(@NonNull Context context, int resource, @NonNull RowItem[] objects) {
        super(context, resource, objects);
        layoutInflater = LayoutInflater.from(context);
    }

    public CustomAdapter(@NonNull Context context, int resource, @NonNull List<RowItem> objects) {
        super(context, resource, objects);
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        RowItem rowItem = getItem(position);

        // If view is not inflated yet then inflate view
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_custom, parent, false);
        }

        // Find views to set data
        AppCompatImageView ivRowImage = convertView.findViewById(R.id.ivRowImage);
        AppCompatTextView tvRowTitle = convertView.findViewById(R.id.tvRowTitle);

        // Set data
        ivRowImage.setImageResource(rowItem.getImageResId());
        tvRowTitle.setText(rowItem.getTitle());

        return convertView;
    }
}
