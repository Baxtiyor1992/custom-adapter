package uz.bakhtiyor.begmatov.customadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Dummy data source
        SampleDataSource dataSource = new SampleDataSource();

        CustomAdapter adapter = new CustomAdapter(this, R.layout.list_item_custom);
        ListView itemsList = findViewById(R.id.itemsList);

        itemsList.setAdapter(adapter);

        // Add rows to adapter
        adapter.addAll(dataSource.getRowItems());

        // Notify list about new rows has been added
        adapter.notifyDataSetChanged();
    }
}
