package uz.bakhtiyor.begmatov.customadapter;

/**
 * Created by Bakhtiyor Begmatov on 1/12/18.
 */

public class RowItem {

    private final int imageResId;
    private final String title;

    public RowItem(int imageResId, String title) {
        this.imageResId = imageResId;
        this.title = title;
    }

    public int getImageResId() {
        return imageResId;
    }

    public String getTitle() {
        return title;
    }
}
