package uz.bakhtiyor.begmatov.customadapter;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Bakhtiyor Begmatov on 1/12/18.
 */

public class SampleDataSource {

    public List<RowItem> getRowItems() {
        return Arrays.asList(
            new RowItem(R.mipmap.ic_launcher, "One"),
            new RowItem(R.mipmap.ic_launcher, "Two"),
            new RowItem(R.mipmap.ic_launcher, "Three"),
            new RowItem(R.mipmap.ic_launcher, "Four"),
            new RowItem(R.mipmap.ic_launcher, "Five"),
            new RowItem(R.mipmap.ic_launcher, "Six"),
            new RowItem(R.mipmap.ic_launcher, "Seven"),
            new RowItem(R.mipmap.ic_launcher, "Eight"),
            new RowItem(R.mipmap.ic_launcher, "Nine"),
            new RowItem(R.mipmap.ic_launcher, "Ten")
        );
    }
}
