package uz.bakhtiyor.begmatov.customadapter;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Bakhtiyor Begmatov on 1/12/18.
 */

public class SplashScreenActivity extends AppCompatActivity {

    private static final long SPLASH_SCREEN_SHOW_DELAY = 2000L;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Start main activity after some time interval
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        openApp();
                    }
                },
                SPLASH_SCREEN_SHOW_DELAY
        );
    }

    private void openApp() {
        MainActivity.start(this);
        finish();
    }
}
